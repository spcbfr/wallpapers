## Wallpapers
My wallpapers collection, these wallpapers are not mine, they are either from [r/wallpapers](https://reddit.com/r/wallpapers) or [r/unixporn](https://reddit.com/r/wallpapers) or somewhere else

## Usage
Basically clone the repository, and use the wallpapers however you like
Personally I use the `setbg` script in my main [dotfiles](https://gitlab.com/spcbfr/dotfiles/-/blob/master/.local/bin/setbg) repo (which is borrowed from larbs) to set and manage my wallpapers wallpaper
